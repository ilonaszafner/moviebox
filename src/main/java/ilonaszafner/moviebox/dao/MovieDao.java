package ilonaszafner.moviebox.dao;

import ilonaszafner.moviebox.model.Movie;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.List;

public interface MovieDao {

    void addMovie(Movie movie);
    List<Movie> showAllMovie ();
    List<Movie> searching(Movie movie);


}
