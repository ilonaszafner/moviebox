package ilonaszafner.moviebox.dao;

import ilonaszafner.moviebox.model.Movie;
import ilonaszafner.moviebox.model.MovieDatabase;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.ArrayList;
import java.util.List;

public class MovieDaoImplementation implements MovieDao {

    List<Movie> moviesList = new MovieDatabase().generateMoviesList();


    @Override
    public void addMovie(Movie movie) {
        if(movie.getTitle() != null) {
            moviesList.add(movie);
        }
    }

    @Override
    public List<Movie> showAllMovie() {
        return moviesList;
    }

    @Override
    public List<Movie> searching(Movie movie) {
        List<Movie> newSearchingList = new ArrayList<>();
        for (int i = 0; i < moviesList.size(); i++) {
            if (moviesList.get(i).getTitle().equalsIgnoreCase(movie.getTitle())  ||
                    (moviesList.get(i).getPremiere() == movie.getPremiere() && Character.isDigit(movie.getPremiere())) ||
                    moviesList.get(i).getDirection().equalsIgnoreCase(movie.getDirection()) ||
                    moviesList.get(i).getScenario().equalsIgnoreCase(movie.getScenario()) ||
                    moviesList.get(i).getType().equalsIgnoreCase(movie.getType()) ||
                    moviesList.get(i).getProduction().equalsIgnoreCase(movie.getProduction())) {
                newSearchingList.add(moviesList.get(i));
            }
        }
        return null;
    }

}

        /*
        try {
            for (int i = 0; i < moviesList.size(); i++) {
                if (moviesList.get(i).getTitle().equalsIgnoreCase(movie.getTitle()) ||
                        (moviesList.get(i).getPremiere() == movie.getPremiere() && Character.isDigit(movie.getPremiere())) ||
                        moviesList.get(i).getDirection().equalsIgnoreCase(movie.getDirection()) ||
                        moviesList.get(i).getScenario().equalsIgnoreCase(movie.getScenario()) ||
                        moviesList.get(i).getType().equalsIgnoreCase(movie.getType()) ||
                        moviesList.get(i).getProduction().equalsIgnoreCase(movie.getProduction())) {
                    newSearchingList.add(moviesList.get(i));
                }

            }
        } catch (Exception exception) {
            System.out.println("Podałeś niepoprawne dane. Zwracam pusty wynik");
        }
    */
