package ilonaszafner.moviebox.model;

public class Movie {

    private String title; // tytuł
    private int premiere; // data premiery
    private String direction; // reżyseria
    private String scenario; // sceneriusz
    private String type; // gatunek
    private String production; // kraj pochodzenia

    public Movie() {
    }

    public Movie(String title, int premiere, String direction, String scenario, String type, String production) {
        this.title = title;
        this.premiere = premiere;
        this.direction = direction;
        this.scenario = scenario;
        this.type = type;
        this.production = production;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPremiere() {
        return premiere;
    }

    public void setPremiere(int premiere) {
        this.premiere = premiere;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getScenario() {
        return scenario;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getProduction() {
        return production;
    }

    public void setProduction(String production) {
        this.production = production;
    }

    @Override
    public String toString() {
        return  title + ": " +
                premiere + ", " +
                direction + ", " +
                scenario + ", " +
                type + ", " +
                production + ".";
    }
}
