package ilonaszafner.moviebox.model;

import java.util.ArrayList;
import java.util.List;

public class MovieDatabase {

    public List<Movie> generateMoviesList(){
    List<Movie> moviesList = new ArrayList<>();
        moviesList.add(new Movie("Planeta Singli", 2016, "Mitja Okorn", "Sam Akina", "Komedia romantyczna", "Polska"));
        moviesList.add(new Movie("Planeta Singli 2", 2018, "Sam Akina", "Sam Akina", "Komedia romantyczna", "Polska"));
        moviesList.add(new Movie("Łowca Jeleni", 1978, "Michael Cimino", "Deric Washburn", "Dramat wojenny", "USA"));
        moviesList.add(new Movie("Terminator", 1984, "James Cameron", "James Cameron", "Akcja", "USA"));
        moviesList.add(new Movie("Fantastyczne zwierzęta i jak je znaleźć", 2016, "David Yates", "J.K. Rowling", "Fantasy", "Wielka Brytania"));
        moviesList.add(new Movie("Gwiezdny pył", 2007, "Mathew Vaughn", "Jane Goldman", "Fantasy", "USA"));
        moviesList.add(new Movie("Poznaj moich rodziców", 2004, "Jay Roach", "Jim Herzfelt", "Komedia", "USA"));
        moviesList.add(new Movie("Depresja gangstera", 1999, "Harold Ramiz", "Harold Ramiz", "Komedia kryminalna", "Australia"));
        moviesList.add(new Movie("Ojciec chrzestny", 1972, "Francis Ford Coppola", "Mario Puzo", "Dramat", "USA"));
        moviesList.add(new Movie("Bohemian Rhapsody", 2018, "David Yates", "J.K. Rowling", "Fantasy", "USA"));
        moviesList.add(new Movie("Fantastyczne zwierzęta: Zbrodnie Grindelwalda", 2018, "Bryan Singer", "Anthony McCarten", "Dramat", "USA"));
        moviesList.add(new Movie("Łowca androidów", 1982, "Ridley Scott", "Hampton Fancher", "Thriller", "Honkong"));
        moviesList.add(new Movie("Jak wytresować smoka 3", 2019, "Dean DeBlois", "Dean DeBlois", "Animacja", "USA"));
        moviesList.add(new Movie("Podziemny krąd", 1999, "David Fincher", "Jim Uhls", "Thriller", "Niemcy"));
        moviesList.add(new Movie("Król Lew", 1994, "Rob Minkoff", "Jonathan Roberts", "Animacja", "USA"));
        moviesList.add(new Movie("Resident Evil", 2002, "Paul W.S.Anderson", "Paul W.S.Anderson", "Horror","Francja"));
    return moviesList;
    }
}
