package ilonaszafner.moviebox.controller;

import ilonaszafner.moviebox.dao.MovieDaoImplementation;
import ilonaszafner.moviebox.model.Movie;
import ilonaszafner.moviebox.model.MovieDatabase;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Controller
public class Controller {

    static List<Movie> movieList = new MovieDaoImplementation().showAllMovie();

    @GetMapping("/")
    public String homeSite() {
        return "index";
    }

    @GetMapping("/add")
    public String addMovie(ModelMap map) {
        return "add";
    }

    @GetMapping("/added")
    public String showedMovie(@ModelAttribute Movie movie, ModelMap map) {
        map.put("addmovie", movieList.add(movie)); // TO TRZEBA POPRAWIĆ
        map.put("movies", movieList);
        return "added";
    }


    @GetMapping("/result")
    public String resultSearching(@RequestParam String title, ModelMap map) {
        List<Movie> newSearchingList = new ArrayList<>();

        for (int i = 0; i < movieList.size(); i++) {
            if (movieList.get(i).getTitle().contains(title) /* ||
                    movieList.get(i).getDirection().contains(title) ||
                    movieList.get(i).getScenario().contains(title) ||
                    movieList.get(i).getType().contains(title) ||
                    movieList.get(i).getProduction().contains(title)*/ ) {
                newSearchingList.add(movieList.get(i));
            }
        }
        map.put("result", newSearchingList);
        return "result";
    }

}
